# Enterosignature paper

This directory includes scripts used to compute enterosignatures on the GMR dataset, and scripts for their analysis.
It contains the original [W](https://gitlab.inria.fr/cfrioux/enterosignature-paper/-/blob/main/data/GMR_NMF_results/5_W.tsv) and [H](https://gitlab.inria.fr/cfrioux/enterosignature-paper/-/blob/main/data/GMR_NMF_results/5_H.tsv) matrices obtained from NMF for the GMR dataset. These are the two matrices used to create most of the figures in the paper.
Their respective row and columns names are those of the [matrix used for NMF decomposition on the GMR dataset](https://gitlab.inria.fr/cfrioux/enterosignature-paper/-/blob/main/data/GMR_dataset/GMR_genus_level_abundance_normalised.tsv).

# Install

scikit-learn v0.24.1 was used in the manuscript (python 3.7). 

Versions of the main packages used:

```
numpy==1.21.6
scikit-learn==0.24.1
scipy==1.7.3
```

# Data

All data and results associated to the manuscript are stored on Zenodo: [https://zenodo.org/record/7923559](https://zenodo.org/record/7923559)

## Computation of Enterosignatures
Enterosignatures (ES) are computed using Scikit-Learn. 
Python scripts are located in `ES_computation`.
The genus-level abundance matrix is **normalised** prior ES computation or bicross-validation. 

### 9-fold Bicross-validation

Two scripts in `ES_computation/Bicross_validation/` permit the following:
- `bicv_rank.py`: determining the best number of enterosignatures for a range of ranks. Outputs consist in several quality scores computed for each run and each fold of cross-validation.

```
python ES_computation/Bicross_validation/bicv_rank.py -h
usage: bicv_rank.py [-h] [--data DATA] [--reps REPS] [--nbruns NBRUNS]
                    [--maxiter MAXITER] [--bottom_k BOTTOM_K] [--top_k TOP_K]
                    [--cpu CPU] [--output OUTPUT] [--seed SEED]

ES 9 fold bicross-validation - rank

optional arguments:
  -h, --help           show this help message and exit
  --data DATA          file_name.tsv - n x m normalised abundance matrix: n
                    	rows are taxa, m columns are samples. Tab-separated.
  --reps REPS          number of matrix shuffling
  --nbruns NBRUNS      number of repeats per rank
  --maxiter MAXITER    max number of iterations per run
  --bottom_k BOTTOM_K  lower boundary
  --top_k TOP_K        upper boundary
  --cpu CPU            number of workers
  --output OUTPUT      output directory
  --seed SEED          numpy seed
```

In the manuscript, **seed** `777` was used.

- `bicv_regu.py`:determining the correct value alpha of L1 regularisation.

```
python ES_computation/Bicross_validation/bicv_regu.py -h
usage: bicv_regu.py [-h] [--data DATA] [--reps REPS] [--nbruns NBRUNS]
                    [--maxiter MAXITER] [--bottom_k BOTTOM_K] [--top_k TOP_K]
                    [--cpu CPU] [--output OUTPUT] [--l1ratio L1RATIO]
                    [--regu {components,transformation,both}] [--seed SEED]

ES 9 fold bicross-validation - L1 regularisation

optional arguments:
  -h, --help            show this help message and exit
  --data DATA           file_name.tsv - n x m normalised abundance matrix: n
                        rows are taxa, m columns are samples. Tab-separated.
  --reps REPS           number of matrix shuffling
  --nbruns NBRUNS       number of repeats per rank
  --maxiter MAXITER     max number of iterations per run
  --bottom_k BOTTOM_K   lower boundary of ES number
  --top_k TOP_K         upper boundary of ES number
  --cpu CPU             number of workers
  --output OUTPUT       output directory
  --l1ratio L1RATIO     L1-L2 regularization ratio
  --regu {components,transformation,both}
                        matrices regularization applies to
  --seed SEED           numpy seed
``` 

In the manuscript, **seed** `159753` was used.

In addition to these scripts, python notebooks in `ES_computation/Bicross_validation/bicv_analysis` provide way to represent and analyse the results of the bicross-validation.

### ES computation
Once parameters are chosen, NMF can be computed on the whole dataset using the script `enterosignature_nmf.py`. 

```
python ES_computation/enterosignatures_nmf.py --help
usage: enterosignatures_nmf.py [-h] -m MATRIX -k K -o OUTPUT [-d {kl,fb}]
                               [-l L1] [-i ITER] [-r {w,h,both,none}]
                               [-n NRUNS] [-a ALPHA]

optional arguments:
  -h, --help            show this help message and exit
  -m MATRIX, --matrix MATRIX
                        abundance matrix file: n x m normalised abundance matrix. n
                        rows are taxa, m columns are samples. Tab-separated.
      					Rownames and colnames are EXPECTED
  -k K, --k K           number of signatures
  -o OUTPUT, --output OUTPUT
                        output directory
  -d {kl,fb}, --divergence {kl,fb}
                        chosen divergence 'kl' or 'fb'
  -l L1, --l1 L1        L1 l2 regularization ratio
  -i ITER, --iter ITER  number of iterations
  -r {w,h,both,none}, --regu {w,h,both,none}
                        regularization on 'w' or 'h' or 'both'
  -n NRUNS, --nruns NRUNS
                        number of runs
  -a ALPHA, --alpha ALPHA
                        alpha parameter of regu
```

### Reapplying existing ES to a new matrix.

This consists in using the W matrix describing the genus-level composition of ES and determining the relative abundance of the corresponding ES in a new abundance matrix, i.e. obtaining the H matrix.
In practice, we run NMF with two inputs: the abundance (normalized) matrix of the new dataset, and the W matrix that was previously calculated. NMF in that case solves a non-negative least square problem. 

This requires the abundance matrix and W matrix to possess the same rows, meaning that a step of homogenisation between the contents of the ES (W matrix) - available in `data/GMR_NMF_results/5_W.tsv` with row names similar to `data/GMR_dataset/GMR_genus_level_abundance_normalised.tsv`  - and the observed genera in the new dataset (abundance matrix) is necessary.

An example of such a preparation is available in `reapply_existing_ES/prepare_matrices.R`.
This script ** needs to be adapted to your needs and your matrices **. 

Then, the script `reapply_nmf.py` can be run. 
Test data and an example of command line are provided in `reapply_existing_ES/test`.

```
python reapply_existing_ES/reapply_nmf.py -h
usage: reapply_nmf.py [-h] -m MATRIXAB -w MATRIXW -o OUTPUT [-d {kl,fb}]
                      [-l L1] [-i ITER] [-r {w,h,both,none}]
                      [-a ALPHA]

optional arguments:
  -h, --help            show this help message and exit
  -m MATRIXAB, --matrixAb MATRIXAB
                        abundance matrix file: n x m normalised abundance matrix. n
                        rows are taxa, m columns are samples. Tab-separated.
						Rownames and colnames are EXPECTED
  -w MATRIXW, --matrixW MATRIXW
                        W matrix file
  -o OUTPUT, --output OUTPUT
                        output directory
  -d {kl,fb}, --divergence {kl,fb}
                        chosen divergence 'kl' or 'fb'
  -l L1, --l1 L1        L1 l2 regularization ratio
  -i ITER, --iter ITER  number of iterations
  -r {w,h,both,none}, --regu {w,h,both,none}
                        regularization on 'w' or 'h' or 'both'
  -n NRUNS, --nruns NRUNS
                        number of runs
  -a ALPHA, --alpha ALPHA
                        alpha parameter of regu
```

## Data available in this repository

The following data is made available to test scripts and reproduce figures presented in the manuscript:

- `data/GMR_dataset`: 
    - `GMR_genus_level_abundance.tsv`: genus-level abundance of the GMR dataset. Non normalised. Used in most R scripts for analysis where it is normalised
  - `GMR_genus_level_abundance_normalised.tsv`: normalised genus-level abundance of the GMR dataset. This is the matrix that was used for NMF decomposition. 
    - `GMR_MGS_gtdbtk_taxonomy.tsv`: taxonomy of MGS reconstructed to the dataset

- `data/NMF_results`: de novo calculation of ES on the GMR matrix. The number of ES used in the paper is 5.
	- `5_H.tsv` is the non-normalised matrix of ES relative abundance in samples
	- `5_W.tsv` is the non-normalised composition of ES in terms of genera. 
	- Normalised versions of the matrix are provided in the supplementary table 1 of the paper
  	- Row names of the `W` matrices are row names of the abundance table `data/GMR_dataset/GMR_genus_level_abundance_normalised.tsv`
  	- Column names of the `H` matrices are column names of the abundance table `data/GMR_dataset/GMR_genus_level_abundance_normalised.tsv`

Metadata is published in 
    Hildebrand, F. et al. Dispersal strategies shape persistence and evolution of human gut bacteria. Cell Host Microbe (2021) [https://doi.org/10.1016/j.chom.2021.05.008](doi:10.1016/j.chom.2021.05.008).

The following scripts expect age categories to be present in the metadata. We provide an additional metadata table (`data/GMR_dataset/age_metadata.tsv`) that can be merged with the one from the above paper and contains age-related variables. 

Additional data is also available on [Zenodo](https://zenodo.org/record/7923559).

## ES analysis

R scripts are located in `ES_analysis`. They permit the generation of analyses and figures used in the paper.

### Analysis of ES composition for a set of ranks

The script `extract_ES.R`. This script uses the outputs provided by the python script computing ES. By default, it looks for NMF results for ranks ranging from 2 to 10. For each rank, the composition of ES will be extracted, figures will be plotted. In addition, H and W matrices describing the composition of ES and the relative (non-normalised) abundance of ES in samples will be produced. These matrices consistute the main outputs of the other analysis scripts. 


```
Rscript --vanilla extract_ES.R
Usage: extract_ES.R [options]


Options:
	-n NMF, --nmf=NMF
		path to NMF result matrices

	-o OUT, --out=OUT
		path to output directory

	-m METADATA, --metadata=METADATA
		path to metadata file

	-a ABUNDANCE, --abundance=ABUNDANCE
		path to abundance matrix (used for NMF)

	-h, --help
		Show this help message and exit
```

### Getting ES representatives (MGS)

The script `MGS_representatives_of_ES.R`. This script uses the outputs provided by the python script computing ES as well as a taxonomy of the MGS from the dataset on which ES were calculated. It writes a tabulated file describing the MGS representatives of each ES. The threshold x below which genera are no longer considered representatives of ES can be modified as an option.  

```
Rscript --vanilla MGS_representatives_of_ES.R
Usage: MGS_representatives_of_ES.R [options]


Options:
	--wmatrix=WMATRIX
		path to W matrix (taxa x ES)

	--hmatrix=HMATRIX
		path to H matrix (ES x samples)

	-o OUT, --out=OUT
		path to output directory

	-x XTHRESHOLD, --xthreshold=XTHRESHOLD
		genera assignment to ES threshold

	-t TAXO, --taxo=TAXO
		path to file mapping MGS to their taxonomy

	-h, --help
		Show this help message and exit
```

### Evolution of ES relative abundance with age

The script `ES_and_age.R` builds sliding windows of age and calculates the average relative abundance of each ES by window. 

```
Rscript --vanilla ES_and_age.R
Usage: ES_and_age.R [options]


Options:
	--hmatrix=HMATRIX
		path to H matrix (ES x samples)

	-o OUT, --out=OUT
		path to output directory

	-m METADATA, --metadata=METADATA
		path to metadata file

	-a ABUNDANCE, --abundance=ABUNDANCE
		path to abundance matrix (used for NMF)

	-h, --help
		Show this help message and exit
``` 

### Number of ES characterising samples

The script `ES_diversity_by_sample` provides plots illustrating the composition of samples (number of ES)

```
Rscript --vanilla ES_diversity_by_sample.R 
Usage: ES_diversity_by_sample.R [options]


Options:
	--wmatrix=WMATRIX
		path to W matrix (taxa x ES)

	--hmatrix=HMATRIX
		path to H matrix (ES x samples)

	-o OUT, --out=OUT
		path to output directory

	-m METADATA, --metadata=METADATA
		path to metadata file

	-a ABUNDANCE, --abundance=ABUNDANCE
		path to abundance matrix (used for NMF)

	-h, --help
		Show this help message and exit
```

### ES associations in samples

The script `ES_associations.R` illustrates the frequency of each ES association in samples while considering that a sample is described by the _n_ most prevalent ES that represent at least 90% of accumulated relative abundance. Plot are drawn for categories of samples (by age, treatment etc.).

```
Rscript --vanilla ES_associations.R
Usage: ES_associations.R [options]


Options:
	--hmatrix=HMATRIX
		path to H matrix (ES x samples)

	-o OUT, --out=OUT
		path to output directory

	-m METADATA, --metadata=METADATA
		path to metadata file

	-h, --help
		Show this help message and exit
```

### Transitions between ES and stability of ES

The script `ES_transition_stability.R` creates plots illustrating how primary ES are stable in individuals sampled longitudinally and which transitions are the most frequent between ES. Plots are drawn by categories of individuals (age, treatment etc.).

```
Rscript --vanilla ES_transition_stability.R
Usage: ES_transition_stability.R [options]


Options:
	--hmatrix=HMATRIX
		path to H matrix (ES x samples)

	-o OUT, --out=OUT
		path to output directory

	-m METADATA, --metadata=METADATA
		path to metadata file

	-h, --help
		Show this help message and exit
```

### Persistence of ES in longitudinal samples

The scripts `primaryES_persistence.R`, `ES_persistence` and `ES_overall_persistence_summary` analyse the 'survival' of ES (present at > 1% relative abundance) or primary ES in individuals sampled longitudinally. The thrid script takes as input tables created by the first two scripts to draw the persistence of ES in infants, adults and elders.

```
Rscript --vanilla ES_persistence.R
Usage: ES_persistence.R [options]


Options:
	--hmatrix=HMATRIX
		path to H matrix (ES x samples)

	-o OUT, --out=OUT
		path to output directory

	-m METADATA, --metadata=METADATA
		path to metadata file

	-h, --help
		Show this help message and exit
```

```
Rscript --vanilla primaryES_persistence.R
Usage: primaryES_persistence.R [options]


Options:
	--hmatrix=HMATRIX
		path to H matrix (ES x samples)

	-o OUT, --out=OUT
		path to output directory

	-m METADATA, --metadata=METADATA
		path to metadata file

	-h, --help
		Show this help message and exit
```

```
Rscript --vanilla ES_overall_persistence_summary.R
Usage: ES_overall_persistence_summary.R [options]


Options:
	--presence=PRESENCE
		path file (presence_ES_survival_by_agecat.tsv) describing the persistence of the presence of ES in each sample

	--primary=PRIMARY
		path file (primary_ES_survival_by_agecat.tsv) describing the persistence of primary ES in each sample

	-o OUT, --out=OUT
		path to output directory

	-m METADATA, --metadata=METADATA
		path to metadata file

	-h, --help
		Show this help message and exit
```

### Metabolic potential of representative MGS (metagenomic species) associated to ES

The script `ES_metabolism.R` permits the assessment and comparison of the metabolic potential of MGS (molecules they can produce from available nutrients) in two settings:
- each MGS considered in a community consisting of the representative MGS of the ES it belongs to: complementarity and redundancy of function **within an ES**
- each MGS considered in a community consisting of representative MGS of **all** ES: complementarity and redundancy of function **between ES**.

All inputs to this script are available in `data/metabo_data` and `data/GMR_dataset`(taxonomy of MGS).

```
	Rscript --vanilla ES_metabolism.R -h
	Usage: ES_metabolism.R [options]


	Options:
		--metPotES=METPOTES
			path to tabulated file describing the metabolic potential of all MGS in interaction with other representatives of their ES

		--metPotFullCom=METPOTFULLCOM
			path to tabulated file describing the metabolic potential of all MGS in the full community of representative species

		-o OUT, --out=OUT
			path to output directory

		-r REPR, --repr=REPR
			path to file describing the list of representative taxa by ES

		--onto1=ONTO1
			path to level 1 ontology of metabolic compounds

		--onto2=ONTO2
			path to level 2 ontology of metabolic compounds

		-t TAXONOMY, --taxonomy=TAXONOMY
			path to file describing the taxonomy of MGS

		-h, --help
			Show this help message and exit
```
