import argparse
from os import listdir, makedirs
from os.path import isdir
import numpy as np
import csv
from sklearn.decomposition import NMF
import random

"""
Run Scikit-Learn NMF on a matrix 
with taxa as rows and samples as
columns for enterosignatures computation.
"""

def is_valid_dir(dirpath):
    """Return True if directory exists or can be created (then create it).
    
    Args:
        dirpath (str): path of directory
    Returns:
        bool: True if dir exists, False otherwise
    """
    if not isdir(dirpath):
        try:
            makedirs(dirpath)
            return True
        except OSError:
            return False
    else:
        return True

def get_args():
    """
    Get arguments of the script and call the main function.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--matrix",
                        help="abundance matrix file: n x m normalised abundance matrix. n rows are taxa, m columns are samples. Tab-separated. Rownames and colnames are EXPECTED", required=True)
    parser.add_argument("-k", "--k", type=int,
                        help="number of signatures", required=True)
    parser.add_argument("-o", "--output",
                        help="output directory", required=True)
    parser.add_argument("-d", "--divergence", type=str, choices=['kl', 'fb'],
                        help="chosen divergence 'kl' or 'fb'", default='kb')
    parser.add_argument("-l", "--l1", type=float,
                        help="L1 l2 regularization ratio", default = 0)
    parser.add_argument("-i", "--iter", type=int,
                        help="number of iterations", default=2000)
    parser.add_argument("-r", "--regu", type=str, choices=['w', 'h', 'both', 'none'],
                        help="regularization on 'w' or 'h' or 'both'", default="none")
    parser.add_argument("-n", "--nruns", type=int,
                        help="number of runs", default=100)
    parser.add_argument("-a", "--alpha", type=float,
                        help="alpha parameter of regu", default=0)

    args = parser.parse_args()

    run_nmf(args.matrix, args.k, args.output, args.alpha, args.divergence, args.l1, args.iter, args.regu, args.nruns)

def cosine_sim(M0, M1):
    """Calculates the cosine similarity between two matrices

    Args:
        M0 (matrix): original matrix on which the NMF was performed
        M1 (matrix): resulting matrix obtained from the multiplication of the NMF matrices
    """
    M0c = np.concatenate(M0)
    M1c = np.concatenate(M1)
    return M1c.dot(M0c) / np.sqrt(M1c.dot(M1c) * M0c.dot(M0c))

def run_nmf(matrix_file, nsig, output_dir, alpha, divergence = 'fb', l1ratio = 0, iternb =1000, regu = 'both', nruns = 1000):
    """Run NMF on an abundance matrix

    Args:
        matrix_file (str): file path of abundance matrix used as input
        nsig (str): number of signatures
        output_dir (str): path to output directory
        alpha (float): alpha parameter for regularisation
        divergence (str, optional): divergence method. Defaults to 'fb'.
        l1ratio (float, optional): L1 L2 regularisation ratio. Defaults to 0.
        iternb (int, optional): number of iterations by run. Defaults to 1000.
        regu (str, optional): matrix/ces on witch regularisation is applied. Defaults to 'both'.
        nruns (int, optional): number of runs to be performed. Defaults to 1000.
    """
    random.seed(10)
    nbrun = nruns
    niter = iternb
    initm = "random" # nndsvd not supported when mu solver and nan values, 
    solvernmf = "mu"
    betaloss = "kullback-leibler" if divergence == 'kl' else 'frobenius'
    l1_ratio = l1ratio
    regu_choice = 'components' if regu == 'w' else 'transformation' if regu == 'h' else 'both' if regu == 'both' else None
    if not is_valid_dir(output_dir):
        print(f'{output_dir} is not a valid directory')

    with open(f"{output_dir}/README.txt", "w") as f:
        f.write(f"Scikit-Learn NMF decomposition was performed on {matrix_file}, with {nbrun} runs of max {niter} iterations; init {initm}; and {solvernmf} solver. Beta-loss function is {betaloss}. L1_ratio is {l1_ratio}. Regularization ratio is {alpha} and is performed on {regu_choice}.\n")

    # read matrix
    with open(matrix_file) as f:
        reader = csv.reader(f)
        columns = next(reader)
        columns = columns[0].split('\t')
        colmap = dict(zip(columns, range(len(columns))))
        n_cols = len(f.readline().split("\t"))

    original_matrix = np.matrix(np.genfromtxt(matrix_file, delimiter="\t", skip_header=1, usecols=np.arange(1, n_cols)))

    print(f"Shape of the matrix: {original_matrix.shape}")

    # run nmf
    print("Running nmf...")

    current_evar = 0
    current_model = None
    best_run = None
    current_H = None
    current_W = None

    for i in range(1,nbrun):
        print(f"----- RUN #{i} ------")
        current_run = 0
        while True:
            current_run += 1
            #rdmst = random.randint(0,2**31)
            nmf_model = NMF(n_components=nsig,
                            init="nndsvdar",
                            verbose=False, 
                            beta_loss = betaloss,
                            solver="mu", 
                            alpha = alpha,
                            max_iter=niter, 
                            random_state = None,
                            l1_ratio=l1_ratio,
                            regularization=regu_choice)
            try:
                H_calc = np.transpose(nmf_model.fit_transform(np.transpose(original_matrix)))
                break
            except AssertionError:
                print("There was an Assertion Error")
                pass
        # print(f"{np.transpose(nmf_model.components_).shape} {H_calc.shape}")
        reconstructed_matrix = np.dot(np.transpose(nmf_model.components_),(H_calc))
        # print(reconstructed_matrix.shape)
        evar = 1 - ( np.sum((np.array(original_matrix)-np.array(reconstructed_matrix))**2) / np.sum(np.array(original_matrix)**2))
        cosine = cosine_sim(np.array(original_matrix), np.array(reconstructed_matrix))

        print(f"Run {i} ({nmf_model.n_iter_} iterations) - error value = {nmf_model.reconstruction_err_} - evar = {evar} - cosine = {cosine}\n")
        if evar > current_evar:
            current_model = nmf_model
            current_error = current_model.reconstruction_err_
            best_run = i
            current_H = H_calc
            current_W = np.transpose(current_model.components_)
            current_evar = evar
            current_cosine = cosine

    print(f"H shape: {current_H.shape}\n") # samples x nsig signatures
    print(f"W shape: {current_W.shape}\n") # nsig signatures x features
    print(f"Error in reconstruction: {current_error}\n")
    print(f"Evar: {current_evar}\n")
    print(f"Cosine: {current_cosine}\n")

    # export files
    print("Exporting results...")
    print(f"Number of iterations: {current_model.n_iter_}")
    print(f"Reconstruction error: {current_model.reconstruction_err_}")
    print(f"Explained variance: {current_evar}")
    print(f"Cosine similarity: {current_cosine}")
    print(f"Best run: {best_run}")
    print(f"Number of components: {current_model.n_components_}")
    np.savetxt(f'{output_dir}/{nsig}_H.tsv', current_H, delimiter='\t')
    np.savetxt(f'{output_dir}/{nsig}_W.tsv', current_W, delimiter='\t')

    return

if __name__ == "__main__":
    get_args()