from sklearn.decomposition import NMF, non_negative_factorization
from sklearn.decomposition._nmf import _beta_divergence
from statistics import mean
import sys
import csv
import json
import numpy as np
import datetime
from os import listdir, makedirs
from os.path import isdir
from multiprocessing import Pool
import argparse

"""
Use Scikit-Learn NMF to perform 9fold bicross validation
on a matrix with taxa as rows and samples as
columns for enterosignatures computation.
"""

def cosine_sim(M0, M1):
    """Calculates the cosine similarity between two matrices

    Args:
        M0 (matrix): original matrix on which the NMF was performed
        M1 (matrix): resulting matrix obtained from the multiplication of the NMF matrices
    """
    M0c = np.concatenate(np.array(M0))
    M1c = np.concatenate(np.array(M1))
    return M1c.dot(M0c) / np.sqrt(M1c.dot(M1c) * M0c.dot(M0c))

def evar(M0, M1):
    """Calculates the explained variance metric between two matrices

    Args:
        M0 (matrix): original matrix on which the NMF was performed
        M1 (matrix): resulting matrix obtained from the multiplication of the NMF matrices
    """
    return 1 - ( np.sum((np.array(M0)-np.array(M1))**2) / np.sum(np.array(M0)**2))

def rss_calc(M0, M1):
    """Calculates the rss metric between two matrices

    Args:
        M0 (matrix): original matrix on which the NMF was performed
        M1 (matrix): resulting matrix obtained from the multiplication of the NMF matrices
    """
    return np.sum((np.array(M0)-np.array(M1))**2)

def l2norm_calc(M0, M1):
    """Calculates the l2 norm metric between two matrices

    Args:
        M0 (matrix): original matrix on which the NMF was performed
        M1 (matrix): resulting matrix obtained from the multiplication of the NMF matrices
    """
    return np.sqrt(np.sum((np.array(M0) - np.array(M1))**2))



def biCV_sub(X_real, h = 3):
    """Takes a matrix, shuffles rows and cols
    and creates h * h submatrices for biCV"""
    # copy matrix
    X_shuf = np.matrix.copy(X_real)
    # shuffle rows
    np.random.shuffle(X_shuf)
    # shuffle columns
    np.random.shuffle(np.transpose(X_shuf))
    # cut the hxh submatrices
    nfeatures, nsamples = X_shuf.shape
    chunks_feat = nfeatures // h
    #remainings_feat = nfeatures % h
    chunks_samp = nsamples // h
    #remainings_samp = nsamples % h
    thresholds_feat = [chunks_feat * i for i in range(1,h)]
    thresholds_feat.insert(0,0)
    thresholds_feat.append(nfeatures+1)
    thresholds_samp = [chunks_samp * i for i in range(1,h)]
    thresholds_samp.insert(0,0)
    thresholds_samp.append(nsamples+1)
    # return the 9 matrices
    matrix_nb = [i for i in range(1, h*h + 1)]
    # operate by row and col
    all_sub_matrices = {}
    done = 0
    row = 0
    col = 0
    while row < h:
        while col < h:
            done += 1
            all_sub_matrices[done] = X_shuf[thresholds_feat[row] : thresholds_feat[row+1], 
                                        thresholds_samp[col] : thresholds_samp[col+1]]
            col += 1
        row += 1
        col = 0
    assert(len(all_sub_matrices) == len(matrix_nb))
    return all_sub_matrices

def concat_3x3_mx(m_dict, row1, row2, row3):
    """ Concatenates submatrices based on the
    order given in arguments"""
    r1 = np.concatenate((m_dict[row1[0]], m_dict[row1[1]], m_dict[row1[2]]), axis=1) 
    r2 = np.concatenate((m_dict[row2[0]], m_dict[row2[1]], m_dict[row2[2]]), axis=1)
    r3 = np.concatenate((m_dict[row3[0]], m_dict[row3[1]], m_dict[row3[2]]), axis=1)
    m = np.concatenate((r1, r2, r3), axis = 0)
    return m
    
def biCV_3x3(sm_dict):
    """Starting from 9 submatrices, rearrange
    them to create 9 matrices used for biCV"""
    all_mx = {}
    # m1 (all rows) 123 - 456 - 789
    all_mx[1] = concat_3x3_mx(sm_dict, [1,2,3], [4,5,6], [7,8,9])    
    # m2 312 - 645 - 978
    all_mx[2] = concat_3x3_mx(sm_dict, [3,1,2], [6,4,5], [9,7,8]) 
    # m3 231 - 564 - 897
    all_mx[3] = concat_3x3_mx(sm_dict, [2,3,1], [5,6,4], [8,9,7]) 
    # m4 789 - 123 - 456
    all_mx[4] = concat_3x3_mx(sm_dict, [7,8,9], [1,2,3], [4,5,6])  
    # m5 456 - 789 - 123
    all_mx[5] = concat_3x3_mx(sm_dict, [4,5,6], [7,8,9], [1,2,3])  
    # m6 645 - 978 - 312
    all_mx[6] = concat_3x3_mx(sm_dict, [6,4,5], [9,7,8], [3,1,2])  
    # m7 978 - 312 - 645
    all_mx[7] = concat_3x3_mx(sm_dict, [9,7,8], [3,1,2], [6,4,5])  
    # m8 897 - 231 - 564
    all_mx[8] = concat_3x3_mx(sm_dict, [8,9,7], [2,3,1], [5,6,4])  
    # m9 564 - 897 - 231
    all_mx[9] = concat_3x3_mx(sm_dict, [5,6,4], [8,9,7], [2,3,1])  
    return all_mx
    
def cut_in_four(m, h=3):
    """Takes a matrix and cuts it in 4
    for cross validation following a ratio h
    e.g h = 3: the M1 submatrix for validation
    will have size 1/9 of the matrix"""
    nfeatures, nsamples = m.shape
    chunks_feat = nfeatures // h
    chunks_samp = nsamples // h
    thresholds_feat = [chunks_feat * i for i in range(1,h)]
    thresholds_samp = [chunks_samp * i for i in range(1,h)]
    m1 = m[0:thresholds_feat[0], 0:thresholds_samp[0]]
    m2 = m[0:thresholds_feat[0], thresholds_samp[0]:nsamples+1]
    m3 = m[thresholds_feat[0]:nfeatures+1, 0:thresholds_samp[0]]
    m4 = m[thresholds_feat[0]:nfeatures+1, thresholds_samp[0]:nsamples+1]
    return m1, m2, m3, m4

def run_cv(mx9, ranks, maxiter = 2000, nruns = 10, h = 3):
    """Runs cross validation for a set of 9 matrices corresponding 
    to shuffled versions of the original matrix in which each 1/9 fold
    becomes the validation set. 
    The NMF algorithm is applied to all 9 matrices for each rank k 
    (number of enterosignatures).
    For each combination of matrix and k, the NMF algorithm is run nruns times.
    For each run, the values of reconstruction error, cosine similarity, L2 norm, RSS, 
    and explained variance are calculated and returned in dictionaries.
    """
    res = {i: None for i in range(1,10)}
    rss = {i: None for i in range(1,10)}
    reco_error = {i: None for i in range(1,10)}
    cosine = {i: None for i in range(1,10)}
    l2_norm = {i: None for i in range(1,10)}
    for i in range(1,len(mx9)+1):
        mx = mx9[i]
        M1, M2, M3, M4 = cut_in_four(mx, h)
        res[i] = {r: {"A":[], "B":[], "C":[], "D":[]} for r in ranks}
        rss[i] = {r: {"A":[], "B":[], "C":[], "D":[]} for r in ranks}
        reco_error[i] = {r: {"A":[], "B":[], "C":[], "D":[]} for r in ranks}
        cosine[i] = {r: {"A":[], "B":[], "C":[], "D":[]} for r in ranks}
        l2_norm[i] = {r: {"A":[], "B":[], "C":[], "D":[]} for r in ranks}
        for rank in ranks:
            for run in range(1,nruns+1):
                # step 1, NMF for M_d
                model_D = NMF(n_components=rank,
                            init="nndsvdar",
                            verbose=False, 
                            solver="mu", 
                            max_iter=maxiter,
                            random_state = None,
                            #l1_ratio = 0.5,
                            alpha = 0,
                            #regularization = "components",
                            beta_loss = "kullback-leibler") #kullback-leibler or frobenius

                H_d = np.transpose(model_D.fit_transform(np.transpose(M4)))
                W_d = np.transpose(model_D.components_)
                Md_calc = W_d.dot(H_d)
                #res[rank]["D"] = {"M":M4, "M'":Md_calc, "evar":evar_M_d}
                res[i][rank]["D"].append(evar(M4, Md_calc))
                reco_error[i][rank]["D"].append(model_D.reconstruction_err_)
                rss[i][rank]["D"].append(rss_calc(M4, Md_calc))
                l2_norm[i][rank]["D"].append(l2norm_calc(M4, Md_calc))
                cosine[i][rank]["D"].append(cosine_sim(M4, Md_calc))
                #print(f"k = {rank} - Evar M_d - {evar_M_d}")
                # step 2, get W_a using M_b
                W_a, H_d_t, n_iter = non_negative_factorization(M2, 
                                                    n_components=rank, 
                                                    init='custom',
                                                    verbose=False,
                                                    solver="mu", 
                                                    max_iter=2000,
                                                    random_state=None,
                                                    alpha = 0,
                                                    beta_loss = "kullback-leibler",
                                                    update_H=False, 
                                                    H=H_d)

                Mb_calc = np.dot(W_a, H_d)
                #res[rank]["B"] = {"M":M2, "M'":Mb_calc, "evar":evar_M_b}
                res[i][rank]["B"].append(evar(M2, Mb_calc))
                # print shapes
                # print(f"shape M2: {M2.shape} -- shape W_a: {W_a.shape} -- shape H_d: {H_d.shape}")
                reco_error[i][rank]["B"].append(_beta_divergence(np.array(M2), np.array(W_a), np.array(H_d), "kullback-leibler",
                                                    square_root=True))
                rss[i][rank]["B"].append(rss_calc(M2, Mb_calc))
                l2_norm[i][rank]["B"].append(l2norm_calc(M2, Mb_calc))
                cosine[i][rank]["B"].append(cosine_sim(M2, Mb_calc))
                # Step 3, get H_a using M_c
                H_a, W_d_t, n_iter = non_negative_factorization(M3.T, 
                                                    n_components=rank, 
                                                    init='custom',
                                                    verbose=False,
                                                    solver="mu", 
                                                    max_iter=2000,
                                                    random_state=None,
                                                    alpha = 0,
                                                    beta_loss = "kullback-leibler",
                                                    update_H=False, 
                                                    H=W_d.T)


                Mc_calc = np.dot(W_d, H_a.T)
                res[i][rank]["C"].append(evar(M3, Mc_calc))
                reco_error[i][rank]["C"].append(_beta_divergence(np.array(M3), np.array(W_d), np.array(H_a).T, "kullback-leibler",
                                                    square_root=True))
                rss[i][rank]["C"].append(rss_calc(M3, Mc_calc))
                l2_norm[i][rank]["C"].append(l2norm_calc(M3, Mc_calc))
                cosine[i][rank]["C"].append(cosine_sim(M3, Mc_calc))
                # step 4, calculate error for M_a
                Ma_calc = np.dot(W_a, H_a.T)
                res[i][rank]["A"].append(evar(M1, Ma_calc))
                reco_error[i][rank]["A"].append(_beta_divergence(np.array(M1), np.array(W_a), np.array(H_a).T, "kullback-leibler",
                                                    square_root=True))
                rss[i][rank]["A"].append(rss_calc(M1, Ma_calc))
                l2_norm[i][rank]["A"].append(l2norm_calc(M1, Ma_calc))
                cosine[i][rank]["A"].append(cosine_sim(M1, Ma_calc))
    return res, rss, reco_error, cosine, l2_norm

def shuffle_and_cv(M, ranks, maxiter = 2000, nruns = 10, h = 3):
    """
    For each of the n runs,
    Shuffle matrix, cut in 9 and perform CV 
    for all 9 submatrices as a validation set
    """
    submx = biCV_sub(M)
    all_9_mx = biCV_3x3(submx)
    res_dict, rss_dict, reco_error_dict, cosine_dict, l2norm_dict = run_cv(mx9 = all_9_mx,
                    ranks = ranks, 
                    maxiter = maxiter, 
                    nruns = nruns,
                    h = 3)
    return {'evar':res_dict, 'rss':rss_dict, 'reconstruction_error':reco_error_dict, 'cosine':cosine_dict, 'l2norm':l2norm_dict}


def mp_biCV(M, ranks, nbcpu, outdir, reps, nbruns, maxiter, logfile, nruns = 100):
    """
    run cross-validations for a range of ranks
    """
    # run method with pool
    with Pool(nbcpu) as pool:
        all_results = [pool.apply_async(shuffle_and_cv, (M, 
                                        ranks, 
                                        maxiter, 
                                        nbruns,
                                        3)).get() for i in range(reps)]
        pool.close()
        pool.join()
    
    # get results from pool
    # create a list of dicts for each measure
    evar = []
    rss = []
    recoerror = []
    cosine = []
    l2norm = []
    # loop over each shuffling run:
    for run in all_results:
        # run is a dict with each measure as key
        evar.append(run["evar"])
        rss.append(run["rss"])
        recoerror.append(run["reconstruction_error"])
        cosine.append(run["cosine"])
        l2norm.append(run["l2norm"])
    # export the resulting lists
    outfile = f"{outdir}/biCV_evar.json"
    with open(outfile, "w") as f:
        json.dump(evar, f, indent=4)

    outfile = f"{outdir}/biCV_rss.json"
    with open(outfile, "w") as f:
        json.dump(rss, f, indent=4)

    outfile = f"{outdir}/biCV_reco_error.json"
    with open(outfile, "w") as f:
        json.dump(recoerror, f, indent=4)

    outfile = f"{outdir}/biCV_cosine.json"
    with open(outfile, "w") as f:
        json.dump(cosine, f, indent=4)

    outfile = f"{outdir}/biCV_l2norm.json"
    with open(outfile, "w") as f:
        json.dump(l2norm, f, indent=4)

    # res = [x.get() for x in res]
    with open(logfile, "a") as f:
        f.write(f"\nResults are written in: {outdir}/biCV.json")
        f.write(f"\nResults are written in: {outdir}/biCV_rss.json")
        f.write(f"\nResults are written in: {outdir}/biCV_reco_error.json")
        f.write(f"\nResults are written in: {outdir}/biCV_cosine.json")
        f.write(f"\nResults are written in: {outdir}/biCV_l2norm.json")

def is_valid_dir(dirpath):
    """Return True if directory exists or can be created (then create it).
    
    Args:
        dirpath (str): path of directory
    Returns:
        bool: True if dir exists, False otherwise
    """
    if not isdir(dirpath):
        try:
            makedirs(dirpath)
            return True
        except OSError:
            return False
    else:
        return True

if __name__ == '__main__':
    # parse script parameters
    parser = argparse.ArgumentParser(description='ES 9 fold bicross-validation - rank')
    parser.add_argument('--data', type=str, help='file_name.tsv - n x m normalised abundance matrix: n rows are taxa, '
                                                'm columns are samples. Tab-separated.')
    parser.add_argument('--reps', type=int, default=100, help='number of matrix shuffling')
    parser.add_argument('--nbruns', type=int, default=10, help='number of repeats per rank')
    parser.add_argument('--maxiter', type=int, default=2000, help='max number of iterations per run')
    parser.add_argument('--bottom_k', type=int, default=1, help='lower boundary')
    parser.add_argument('--top_k', type=int, default=10, help='upper boundary')
    parser.add_argument('--cpu', type=int, default=9, help='number of workers')
    parser.add_argument('--output', type=str, help='output directory')
    parser.add_argument('--seed', type=int, help='numpy seed')

    args = parser.parse_args()
    outdir = args.output
    if args.seed:
        np.random.seed(args.seed)
    if not is_valid_dir(outdir):
        print(f'{outdir} is not a valid directory')
    Ks = [i for i in range(args.bottom_k, args.top_k+1)]

    timestamp = datetime.datetime.now().strftime('%d-%m_%H-%M-%S')
    outlog = f"{outdir}/biCV_{args.reps}_reps_{args.maxiter}_maxiters_{args.bottom_k}-{args.top_k}_k_seed_{args.seed}-{timestamp}.txt"
    with open(outlog, "w") as f:
        f.write(str(args))
    # V = np.load(args.data)
    with open(args.data) as f:
        reader = csv.reader(f)
        n_cols = len(f.readline().split("\t"))
    V = np.matrix(np.genfromtxt(args.data, delimiter="\t", skip_header=1, usecols=np.arange(1, n_cols+1)))
    # print shape of V
    print(f"Abundance matrix shape: {V.shape}")
    # bicv
    mp_biCV(V, Ks, args.cpu, outdir, args.reps, args.nbruns, args.maxiter, outlog)
