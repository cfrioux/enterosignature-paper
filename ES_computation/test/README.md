## Running enterosignature decomposition

A very simple example using the matrix of this directory (100 samples, 586 taxa)

```
python ../enterosignatures_nmf.py -k 5 -m abundance_matrix.tsv -o nmf_results -d kl
```

Gives the following output:

```
Shape of the matrix: (586, 100)
Running nmf...
----- RUN #1 ------
Run 1 (120 iterations) - error value = 9.87988064078033 - evar = 0.854373648806427 - cosine = 0.9245250674933956

----- RUN #2 ------
Run 2 (120 iterations) - error value = 9.880450953945635 - evar = 0.8544602996063961 - cosine = 0.9245793149611874

[...]

----- RUN #98 ------
Run 98 (110 iterations) - error value = 9.88078552847999 - evar = 0.8544634483139868 - cosine = 0.9245761945758734

----- RUN #99 ------
Run 99 (120 iterations) - error value = 9.881635760190013 - evar = 0.8544158842614347 - cosine = 0.9245669768162237

H shape: (5, 100)

W shape: (586, 5)

Error in reconstruction: 9.879615907192337

Evar: 0.8544870039383952

Cosine: 0.924581718903521

Exporting results...
Number of iterations: 120
Reconstruction error: 9.879615907192337
Explained variance: 0.8544870039383952
Cosine similarity: 0.924581718903521
Best run: 20
Number of components: 5
```

An output directory is created, containing the H and W matrices, and a README file summarising the options that were used for the run. 

## Running bicross-validation

A simple and fast run:

```
python ../Bicross_validation/bicv_rank.py --data abundance_matrix.tsv --reps 1 --nbruns 1 --bottom_k 2 --top_k 3 --seed 777 --output bicv
```

For real case computations, use more realistic values for arguments, eg `--reps 200 --maxiter 2000 --nbruns 1 --bottom_k 2 --top_k 30`. Here we test from 2 to 20 enterosignatures. `--nbruns` = 1 is fine, the number of repetitions `--reps` is the option that matters most as it is the number of cross-validations. 

Several json files are outputed. Take a look at the jupyter notebooks in `ES_computation/Bicross_validation/bicv_analysis` for their analysis.
