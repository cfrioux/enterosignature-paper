This directory contains data for reapplying existing enterosignatures to a new dataset.

- `abundance-matrix.tsv` is the normalised abundance data for the new dataset. Rows are features (taxa), columns are samples.
- `existing_ES.tsv` is the composition of the existing enterosignatures we want to map to the new dataset. Rows are features (taxa), columns are enterosignatures. 

The script can be called the following way:

```
python ../reapply_nmf.py -m abundance_matrix.tsv -w existing_ES.tsv -o results -d kl -i 2000
```

Gives the following logs...

```
Shape of the abundance matrix: (586, 1152)
Shape of the W matrix: (586, 5)
Number of signatures k: 5
Running nmf...
----- RUN #1 ------
True
Run 1 (20 iterations) - error value = 161.50517168653897 - evar = 0.5180499857290406

H shape: (1152, 5) (n samples x k signatures)

Error in reconstruction: 161.50517168653897

Evar: 0.5180499857290406

Exporting results...
Reconstruction error: 161.50517168653897
Evar: 0.5180499857290406
Best run: 1
```

... as well as the H and W matrices in the `results` directory. 

Be careful: the script **expects colnames and rownames**. If not provided in the files, one ES will be dropped, and shape errors will occur. 

Please take a look at the main README of the repository for additional information on the versions of the dependencies. 
