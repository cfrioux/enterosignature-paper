import argparse
from os import listdir, makedirs
from os.path import isdir
import numpy as np
import csv
from sklearn.decomposition import NMF, non_negative_factorization
from sklearn.decomposition._nmf import _beta_divergence
import random

"""
Run Scikit-Learn NMF on a matrix 
with taxa as rows and samples as
columns using an existing W matrix.
Purpose = retrive the H matrix
k (number of signatures is given by the 
number of columns in the W matrix)
"""

def is_valid_dir(dirpath):
    """Return True if directory exists or can be created (then create it).
    
    Args:
        dirpath (str): path of directory
    Returns:
        bool: True if dir exists, False otherwise
    """
    if not isdir(dirpath):
        try:
            makedirs(dirpath)
            return True
        except OSError:
            return False
    else:
        return True

def calc_evar(M0, M1):
    """Calculates the explained variance metric between two matrices

    Args:
        M0 (matrix): original matrix on which the NMF was performed
        M1 (matrix): resulting matrix obtained from the multiplication of the NMF matrices
    """
    return 1 - ( np.sum((np.array(M0)-np.array(M1))**2) / np.sum(np.array(M0)**2))

def get_args():
    """
    Get arguments of the script and call the main function.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--matrixAb",
                        help="abundance matrix file: n x m normalised abundance matrix. n rows are taxa, m columns are samples. Tab-separated. Rownames and colnames are EXPECTED", required=True)
    parser.add_argument("-w", "--matrixW",
                        help="W matrix file", required=True)
    parser.add_argument("-o", "--output",
                        help="output directory", required=True)
    parser.add_argument("-d", "--divergence", type=str, choices=['kl', 'fb'],
                        help="chosen divergence 'kl' or 'fb'")
    #parser.add_argument("-l", "--l1", type=float,
    #                    help="L1 l2 regularization ratio")
    parser.add_argument("-i", "--iter", type=int, default=2000,
                        help="number of iterations")
    #parser.add_argument("-r", "--regu", type=str, choices=['w', 'h', 'both', 'none'],
    #                    help="regularization on 'w' or 'h' or 'both'")
    #parser.add_argument("-a", "--alpha", type=float,
    #                    help="alpha parameter of regu")

    args = parser.parse_args()

    run_nmf(args.matrixAb, args.matrixW, args.output, args.divergence, args.iter)

def run_nmf(matrix_file, wmatrix_file, output_dir, divergence = 'fb', iternb =2000, nruns = 1):
    """Run NMF on an abundance matrix using an existing W matrix

    Args:
        matrix_file (str): file path of abundance matrix used as input
        wmatrix_file (str): file path of W matrix used as input
        output_dir (str): path to output directory
        divergence (str, optional): divergence method. Defaults to 'fb'.
        iternb (int, optional): number of iterations by run. Defaults to 2000.
        nruns (int, optional): number of runs to be performed. Defaults to 1.
    """
    random.seed(10)
    # initm = "random" # nndsvd not supported when mu solver and nan values, 
    solvernmf = "mu"
    betaloss = "kullback-leibler" if divergence == 'kl' else 'frobenius'
    #regu_choice = 'components' if regu == 'w' else 'transformation' if regu == 'h' else 'both' if regu == 'both' else None
    if not is_valid_dir(output_dir):
        print(f'{output_dir} is not a valid directory')

    with open(f"{output_dir}/README.txt", "w") as f:
        f.write(f"Scikit-Learn NMF decomposition was performed on {matrix_file} using W matrix {wmatrix_file}, with {nruns} runs of max {iternb} iterations; and {solvernmf} solver. Beta-loss function is {betaloss}. \n")

    # read abundance matrix
    with open(matrix_file) as f:
        reader = csv.reader(f)
        columns = next(reader)
        columns = columns[0].split('\t')
        colmap = dict(zip(columns, range(len(columns))))
        n_cols = len(f.readline().split("\t"))

    abundance = np.matrix(np.genfromtxt(matrix_file, delimiter="\t", skip_header=1, usecols=np.arange(1, n_cols)))

    print(f"Shape of the abundance matrix: {abundance.shape}")

    with open(wmatrix_file) as f:
        reader = csv.reader(f)
        columns = next(reader)
        columns = columns[0].split('\t')
        colmap = dict(zip(columns, range(len(columns))))
        n_cols = len(f.readline().split("\t"))

    W_init = np.matrix(np.genfromtxt(wmatrix_file, delimiter="\t", skip_header=1, usecols=np.arange(1, n_cols)))

    print(f"Shape of the W matrix: {W_init.shape}")

    nsig = W_init.shape[1]

    print(f"Number of signatures k: {nsig}")

    # run nmf
    print("Running nmf...")

    current_evar = 0
    current_model = None
    best_run = None
    current_H = None
    current_W = None

    for i in range(1,nruns+1):
        print(f"----- RUN #{i} ------")
        H_nmf, W_nmf, n_iter = non_negative_factorization(np.array(abundance.T), 
                                            n_components=nsig, 
                                            init='custom',
                                            verbose=False,
                                            solver="mu", 
                                            max_iter=iternb,
                                            random_state=None,
                                            alpha = 0,
                                            beta_loss = "kullback-leibler",
                                            update_H=False, 
                                            H=np.array(W_init.T))
        # test that the W matrix has not changed
        # print(np.array_equal(W_init,W_nmf.T))
        try:
            abundance_calc = np.dot(W_init, H_nmf.T)
            evar = calc_evar(abundance, abundance_calc)
            rec_error = _beta_divergence(np.array(abundance.T), np.array(H_nmf), np.array(W_init.T), betaloss, square_root=True)
        except AssertionError:
            print("There was an Assertion Error")
            pass

        print(f"Run {i} ({n_iter} iterations) - error value = {rec_error} - evar = {evar}\n")
        if evar > current_evar:
            current_error = rec_error
            best_run = i
            current_H = H_nmf
            current_evar = evar

    print(f"H shape: {current_H.shape} (n samples x k signatures) \n") # samples x nsig signatures
    print(f"Error in reconstruction: {current_error}\n")
    print(f"Evar: {current_evar}\n")

    # export files
    print("Exporting results...")
    print(f"Reconstruction error: {current_error}")
    print(f"Evar: {current_evar}")
    print(f"Best run: {best_run}")
    np.savetxt(f'{output_dir}/{nsig}_H.tsv', current_H, delimiter='\t')
    np.savetxt(f'{output_dir}/{nsig}_W.tsv', W_init, delimiter='\t')
    return


if __name__ == "__main__":
    get_args()
